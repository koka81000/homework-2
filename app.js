const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const authRouter = require('./routers/authRouter');
const profileRouter =require('./routers/profileRouter');
const notesRouter = require('./routers/notesRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('short'));

app.use('/api/auth', authRouter );
app.use('/api/users', profileRouter );
app.use('/api/notes', notesRouter );

/** Class representing a statusCode. */
class UnauthorizedError extends Error {
  /**
     * @param {string} message - The message value.
     */
  constructor(message = 'Unauthorized user') {
    super(message);
    statusCode = 400;
  }
}

app.use((err, req, res, next)=>{
  if ( err instanceof UnauthorizedError ) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () =>{
  await mongoose.connect('mongodb+srv://test:test@cluster0.ayawb.mongodb.net/todo', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
  app.listen(8080, () => {
    console.log('Server works at port 8080!');
  });
};

start();
