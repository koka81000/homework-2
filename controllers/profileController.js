const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

module.exports.getProfile = async (req, res) => {
  const userMe = await User.findById(req.user._id).exec();
  return res.status(200).json({
    user: {_id: userMe._id, username: userMe.username,
      createdDate: userMe.createdDate,
    },
  });
};

module.exports.deleteProfile = async (req, res)=>{
  await User.findByIdAndRemove(req.user._id, req.body, (err) => {
    if (!err) {
      return res.status(200).json({message: 'Success'});
    } else {
      res.status(400).json({message: 'Something went wrong'});
    }
  });
};

module.exports.patchProfile = async (req, res)=>{
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById( req.user._id, 'password' ).exec();

  if (!(await bcrypt.compare( oldPassword, user.password ))) {
    return res.status( 400 ).json( {message: 'Wrong password!!!'});
  }
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({message: 'Success'});
};
