const Note = require('../models/noteModel');

module.exports.getNotes = async (req, res) => {
  const {offset, limit} = req.body;
  const notes = await Note.find({userId: req.user._id}, {__v: 0})
      .skip(offset).limit(limit);
  res.status(200).json({notes: notes});
};


module.exports.postNotes = async (req, res) => {
  const udserId = req.user._id;
  const note = new Note({
    userId: udserId,
    completed: false,
    text: req.body.text});
  try {
    await note.save();
    res.status(200).json({message: 'Success!!'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};


module.exports.getNoteById = async (req, res)=>{
  const note = await Note.findById(req.params.id, {__v: 0});
  if (!note) {
    res.status(400).json({message: 'No task found'});
  }
  res.status(200).send({note});
};


module.exports.deleteNoteById = async (req, res) => {
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id});
  if (!note) {
    res.status(400).json({message: 'No task found'});
  }
  await Note.findOneAndDelete({
    _id,
    userId: req.user._id});
  res.status(200).json({message: 'Success'});
};

module.exports.putNoteById = async (req, res)=> {
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id});
  if (!note) {
    res.status(400).json({message: 'No task found'});
  }
  if (req.body.text) {
    if (note.text === req.body.text) {
      res.status(400).json({message: 'Enter new description of your note'});
    } else {
      note.text = req.body.text;
      await note.save();
      res.status(200).json({message: 'Success'});
    }
  } else {
    res.status(400).json({message: 'Enter text field'});
  }
};

module.exports.patchNoteById = async (req, res)=>{
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id});
  if (!note) {
    res.status(400).json({message: 'No task found'});
  }
  note.completed = !note.completed;
  await note.save();
  res.status(200).json({message: 'Success'});
};
