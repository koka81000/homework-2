const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {getNotes,
  postNotes,
  getNoteById,
  deleteNoteById,
  putNoteById,
  patchNoteById} = require('../controllers/notesController');


router.get('', authMiddleware, asyncWrapper(getNotes));

router.post('', authMiddleware, asyncWrapper(postNotes));

router.get('/:id', authMiddleware, asyncWrapper(getNoteById));

router.delete('/:id', authMiddleware, asyncWrapper(deleteNoteById));

router.put('/:id', authMiddleware, asyncWrapper(putNoteById));

router.patch('/:id', authMiddleware, asyncWrapper(patchNoteById));

module.exports = router;
