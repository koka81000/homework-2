const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {getProfile,
  deleteProfile,
  patchProfile,
} = require('../controllers/profileController');

router.get('/me', authMiddleware, asyncWrapper(getProfile));

router.delete('/me', authMiddleware, asyncWrapper(deleteProfile));

router.patch('/me', authMiddleware, asyncWrapper(patchProfile));

module.exports = router;
